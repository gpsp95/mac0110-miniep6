# Parte 2.1 - Escreva abaixo sua função impares_consecutivos(n)

function impares_consecutivos(n)
    if n==1
        return 1
        end
    x=1
    for i = 1:n
        x=(x)+(i-1) 
    end
    c=1+(x-1)*2
        
   return c
end

# Parte 2.2 - Escreva abaixo suas funções imprime_impares_consecutivos(m) e mostra_n(n)

function imprime_impares_consecutivos(m) 
    	print(m)
    	print(" ")
    	print(m^3)
    
    	x=1
    	for i = 1:m
        	x=(x)+(i-1) 
    	end
    
    	for i = -1:m-2
        	print(" ")
        	c=1+(x+i)*2
        	print(c)
    	end
    
    

end

function mostra_n(n)
   for i = 1:n
        println()
        imprime_impares_consecutivos(i)
    end
end

# Testes automatizados - segue os testes para a parte 2.1. Não há testes para a parte 2.2.

function test()
    if impares_consecutivos(1) != 1
        print("Sua função retornou o valor errado para n = 1")
    end
    if impares_consecutivos(2) != 3
        print("Sua função retornou o valor errado para n = 2")
    end
    if impares_consecutivos(7) != 43
        print("Sua função retornou o valor errado para n = 7")
    end
    if impares_consecutivos(14) != 183
        print("Sua função retornou o valor errado para n = 14")
    end
    if impares_consecutivos(21) != 421
        print("Sua função retornou o valor errado para n = 21")
    end
end

# Para rodar os testes, certifique-se de que suas funções estão com os nomes corretos! Em seguida, descomente a linha abaixo:
# test()
